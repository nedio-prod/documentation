# Convention de Nommage des fichiers

## Vidéos

### Film / Clip 

#### Valeur de Cadre

- **PG**    | Plan Général
- **PE**    | Plan d'ensemble (Paysage)
- **PDD**   | Plan demis ensemble
- **PM**    | Plan moyen
- **PA**    | Plan Américan (Plan Western)
- **PMM**   | Plan Rapproché Taille
- **PR**    | Plan Rapproché
- **GP**    | Gros Plan
- **TGP**   | Trés Gros Plan
- **PO**    | Plan Objet
- **I**     | Insert

#### Qualité d'un rush (son ou vidéo)

- Bonne [B] : 
    - image nette, cadrage correct, mouvement ni tremblé, ni saccadé
    - sons identifiables, voix parfaitement compréhensible
- Moyenne [M] : un des paramètres est défaillant
- Faible [F] : plusieurs paramètres sont défaillants

#### Qualité d'un rush (son ou vidéo)

#### Prefix du fichier

Voici à quoi doit ressembler le nom d'un media après le dérushage :

`S{SEQ(2)}-{SC(3)}-{PLAN(3)}_{CADRE(3)}_{QUALITE}_DescriptionRapide_{PRISE(2)}`

- **{Séquence}** est le numéro de la séquence avec 2 chiffre
- **{Scéne}** est le numéra de la scéne avec 3 chiffre
- **{Plan}** le numéro du plan à 3 chiffre
- **{Valeur du cadre}**, se référer au points précédents! Doit contenir 3 caractères MAJUSCULE : `ex: TGP, GP_, I__`
- **{Qualité}** La qualité du cadre avec une lettre (se référer au point précédent)
- **DescriptionRapide** : Description de la scéne rapidement (éviter les mots inutiles comme la, le, un..) `ex : JackSort, JackBoisEtSort, LisaPrendVerre`
- **Prise** : Le numéro de la prise (non obligatoire si qu'une prise)

##### Exemple de media correctement nommé : 
```
S02-004-005_GP__B_DepartAvion_01.mp4
S12-052-010_I___B_TitreChap12_01.mp4
S05-005-010_PDD_M_HangarPanorama.mp4
S05-005-010_PDD_M_HangarPanorama_02.mp4
```
##### Exemple de media mal nommé : 
```
S2-4-5_GP__B_DepartAvion_01.mp4 (les chiffres ne sont pas correctement formé)
S12-052-010_I_B_TitreChap12_01.mp4 (3 Caractères attendus pour le cadrage)
HangarPanorama_02.mp4
```

